select sh.name,sh.launched,cl.displacement
from ships sh
join classes cl on sh.class=cl.class
where exists
(select * from classes where cl.displacement>35000 and cl.type='bb') and launched>=1922;
select distinct pr.maker
from product pr
where exists(select * from pc
where pc.speed>=750 and pr.model=pc.model) or
exists (select * from laptop lt
where lt.speed>=750 and pr.model=lt.model);
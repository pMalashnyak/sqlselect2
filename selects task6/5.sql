select o.point,date(o.date) as date,
case
	when (select max(o1.out) from outcome o1 where date(o1.date)=date(o.date) and o1.point=o.point)>oo.out 
    then 'more than once'
	when o.out<oo.out then 'once'
    else 'both'
end as res
from outcome o
join outcome_o oo on date(o.date)=date(oo.date) and o.point = oo.point;
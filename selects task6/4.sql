select tr.trip_no,c.name,tr.plane,tr.town_from,tr.town_to,
case
	when tr.time_in>tr.time_out then (hour(tr.time_in)-hour(tr.time_out))
    when tr.time_in<tr.time_out then (hour(tr.time_out)-hour(tr.time_in)) 
end time_in_flight
from trip tr
join company c on c.ID_comp=tr.ID_comp
select distinct maker,
case
	when pd.maker not in (select pr.maker from product pr where pr.type='PC') then 'NO'
    when pd.maker in (select pr.maker from product pr where pr.type='PC') then concat('YES(',
    (select count(p.maker) from product p where p.maker=pd.maker and p.type='PC'),')') 
end pc
from product pd
order by pd.maker;
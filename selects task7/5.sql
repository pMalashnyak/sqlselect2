select cl.class
from classes cl
where (select count(sh.name) from ships sh where cl.class=sh.class)=1
UNION
select o.ship
from outcomes o
where o.ship not in (select s.name from ships s);
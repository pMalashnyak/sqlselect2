select sh.name,sh.class
from ships sh
UNION
select o.ship,
case
	when exists(select cl.class from ships sh join classes cl on cl.class=sh.class where sh.name=o.ship) 
    then (select cl.class from ships sh join classes cl on cl.class=sh.class where sh.name=o.ship)
end
from outcomes o
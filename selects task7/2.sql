select distinct 'PC' type, p.model,(select max(p1.price) from pc p1 where p1.model=p.model) max
from pc p 
UNION
select distinct 'Printer' type, pr.model,(select max(pr1.price) from printer pr1 where pr1.model=pr.model) max
from printer pr 
UNION
select distinct 'Laptop' type, l.model,(select max(lt.price) from laptop lt where lt.model=l.model)
from laptop l;
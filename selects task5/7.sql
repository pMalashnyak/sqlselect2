select o.ship,cl.displacement,cl.numGuns
from outcomes o
join ships sh on sh.name=o.ship
join classes cl on cl.class=sh.class
where o.battle='Guadalcanal';
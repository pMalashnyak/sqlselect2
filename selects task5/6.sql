select distinct maker , 
(select avg(p.hd) from product pd join pc p on p.model=pd.model where pd.maker=pr.maker) average
from product pr
where pr.type='PC' and pr.maker in (select pro.maker from product pro where pro.type='Printer')
order by maker;
select outc.ship,outc.battle,btl.date
from outcomes outc
join battles btl on btl.name=outc.battle
where outc.result='damaged' and outc.ship in (select outc2.ship from outcomes outc2
where outc.battle<>outc2.battle);
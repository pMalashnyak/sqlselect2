drop database student;
create database Student;
use Student;

create table Student.group(
id integer primary key auto_increment,
group_number varchar(10) not null,
speciality varchar(45) not null
);

create table raiting(
id integer primary key auto_increment,
raiting_num integer not null,
stependya integer not null
);

create table students(
id integer primary key auto_increment,
stud_name varchar(45) not null,
fotography varchar(45),
autobigraphy varchar(100),
year_of_vstup date not null,
year_of_birth date not null,
home_address varchar(45),
group_id integer not null,
raiting_id integer not null,

foreign key(group_id) references Student.group(id),
foreign key(raiting_id) references raiting(id)
);

create table module(
id integer primary key auto_increment,
grade integer not null);

create table Student.subject(
id integer primary key auto_increment,
subject_name varchar(45) not null,
module1_id integer not null,
module2_id integer not null,
grade_100 double not null,
grade_5 double not null,
type_of_cred varchar(15) not null,
semester_num integer not null,
prof_name varchar(45) not null,
stud_id integer not null,

foreign key(stud_id) references students(id),
foreign key(module1_id) references module(id),
foreign key(module2_id) references module(id),

check (module2_id <> module1_id),
check (grade_5<=5.0 and grade_5>=2.0),
check (grade_100<=100.0 and grade_100>=0.0),
check (semester_num>0 and semester_num<=12),
check (type_of_cred='exam' or type_of_cred='zalik')
);

insert into Student.group (group_number,speciality) values ('ПМП-12','Прикладна');
insert into Student.group (group_number,speciality) values ('ПМП-32','Прикладна');
insert into Student.group (group_number,speciality) values ('ПМП-22','Прикладна');

insert into raiting (raiting_num,stependya) values(2,1200);
insert into raiting (raiting_num,stependya) values(5,1200);
insert into raiting (raiting_num,stependya) values(6,1200);

insert into Student.students (stud_name,fotography,year_of_vstup,year_of_birth,home_address,group_id,raiting_id)
values ('Sample name1','somepath.jpg',20120901,19971122,'samplestreet',1,1);
insert into Student.students (stud_name,autobigraphy,year_of_vstup,year_of_birth,group_id,raiting_id)
values ('Sample name2','some text',20110901,19940102,2,3);
insert into Student.students (stud_name,autobigraphy,year_of_vstup,year_of_birth,group_id,raiting_id)
values ('Sample name3','some text',20120901,19950102,3,2);

insert into module(grade) values(12);
insert into module(grade) values(15);
insert into module(grade) values(15);
insert into module(grade) values(1);

insert into Student.subject(subject_name,module1_id,module2_id,grade_100,grade_5,type_of_cred,
semester_num,prof_name,stud_id) values('some subj1',1,2,90,5,'exam',2,'some prof',1);
insert into Student.subject(subject_name,module1_id,module2_id,grade_100,grade_5,type_of_cred,
semester_num,prof_name,stud_id) values('some subj2',3,4,77,4,'zalik',2,'some prof',3);